"""This module is responsible for the arithmetic calculation concern"""
import math

"""Declare a variable to serve as calculator memory"""
lastResult = 0

def add(num1, num2):
    """Adds two numbers and returns the result"""
    global lastResult
    lastResult = num1 + num2
    return lastResult

def subtract(num1, num2):
    """Subtracts two numbers and returns the result"""
    global lastResult
    lastResult = num1 - num2
    return lastResult

def multiply(num1, num2):
    """Multiplies two numbers and returns the result"""
    global lastResult
    lastResult = num1 * num2
    return lastResult

def divide (num1, num2):
    """Divides two numbers and returns the result"""
    global lastResult
    lastResult = num1 / num2
    return lastResult

def intDiv(num1, num2):
    """Divides two integer numbers and returns the result of the integer division"""
    lastResult = num1 // num2
    return lastResult

def remainder(num1, num2):
    """Calculates the remainder of the integer division operation between the two arguments"""
    global lastResult
    lastResult = num1 % num2
    return lastResult

def exp(num1, num2):
    """Calculates the num1 to the power of num2"""
    global lastResult
    lastResult = num1 ** num2
    return lastResult

def solveQuadratic(a, b, c):
    """Find the roots of a quadratic equation a*x^2 + b*x + c = 0"""
    #find the discriminator
    disc = b ** 2 - 4*a*c
    
    #calculate the positive and the negative root
    xPos = (-b + math.sqrt(disc)) / (2 * a)
    xNeg = (-b - math.sqrt(disc)) / (2 * a)

    #return the roots of the quadratic equation as a tuple
    return (xPos, xNeg)

    