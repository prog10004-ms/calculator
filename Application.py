"""This  module is responsible for the user interactivity concern"""
import Calculator as calc 

#use the Calcululator  module to add two numbers
result = calc.add(5, 17)
print(f'5 + 17 = {result}')

result = calc.subtract(23, 45)
print(f'23 - 45 = {result}')

result = calc.multiply(34, 12)
print(f'34 * 12 = {result}')

result = calc.divide(7, 2)
print(f'Real Number Division: 7 / 2 = {result}')

result = calc.intDiv(7, 2)
print(f'Integer Division: 7 / 2 = {result}')

result = calc.remainder(7,2)
print(f'Remainder of 7 / 2 is {result}')

result = calc.exp(2, 30)
print(f'2 ^ 30 = {result}')

#solve a quadratic equation as provided by the user
print('Please provide the quadratic coeficients for: ax^2 + bx + c = 0')
a = float(input('a = '))
b = float(input('b = '))
c = float(input('c = '))
rPos, rNeg = calc.solveQuadratic(a, b, c)
print(f'The roots of the equations are: {rPos} {rNeg}')

#perform operations using the new ability of the caculator to remember the last result
calc.add(23, 2) 
result = calc.exp(calc.lastResult, 2)
print(f'The result is {result}')

#TODO: add two memory slots in the calculator memory1 and memory2  and add new functionality
#in the calculator to save the last result into memory1 or memory2 slots. Your calculator will now
#be able to use the last result and two saved results in calculations.